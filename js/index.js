$(document).ready(function(){ 
	$('[data-toggle="tooltip"]').tooltip();
	$('[data-toggle="popover"]').popover();
	/*La velocidad del carrusel se hizo directamente en el div _ linea 84 _ es mas facil y practico*/
	$('#contacto').on('show.bs.modal', function (e){
		console.log('Se muestra el modal contacto');
		$('#contactoBtn').removeClass('btn-outline-success');
		$('#contactoBtn').addClass('btn-primary');
		$('#contactoBtn').prop('disabled', true);
	});
	$('#contacto').on('shown.bs.modal', function (e){
		console.log('Se mostró el modal contacto');
	});
	$('#contacto').on('hide.bs.modal', function (e){
		console.log('Se oculta el modal contacto');
	});
	$('#contacto').on('hidden.bs.modal', function (e){
		console.log('Se ocultó el modal contacto');
		$('#contactoBtn').prop('disabled', false);
	});
});


/* El script del curso no funcionó - tomamos codigo de web bootstrap - ver arriba que funciona
       
        <script>
            $(function(){
                $("[data-toggle='tooltip']").tooltip();
            });
        </script>
    
    */